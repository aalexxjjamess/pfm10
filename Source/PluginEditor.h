/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

#define MaxdB 6.f
#define MindB -100.f

//==============================================================================
/**
*/

struct Tick
{
    int y = 0;
    float dB = 0.f;
};

//==============================================================================
struct dBScale : Component
{
    void paint(Graphics& g);
    std::vector<Tick> ticks;
    int yOffset = 0;
};

//==============================================================================
struct Meter : Component
{
    void paint(Graphics&) override;
    void update(float);
    void resized() override;
    std::vector<Tick> ticks;
private:
    float level;
};

//==============================================================================

class Pfmcpp_project10AudioProcessorEditor  : public AudioProcessorEditor, public Timer
{
public:
    Pfmcpp_project10AudioProcessorEditor (Pfmcpp_project10AudioProcessor&);
    ~Pfmcpp_project10AudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;
    void timerCallback() override;
    
    Meter meter;
    dBScale dbscale;
private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Pfmcpp_project10AudioProcessor& processor;
    AudioBuffer<float> pulled;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Pfmcpp_project10AudioProcessorEditor)
};


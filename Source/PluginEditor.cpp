/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
Pfmcpp_project10AudioProcessorEditor::Pfmcpp_project10AudioProcessorEditor (Pfmcpp_project10AudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    startTimerHz(20);
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    addAndMakeVisible(meter);
    addAndMakeVisible(dbscale);
    setSize (400, 300);
    
    startTimerHz(40);
}

Pfmcpp_project10AudioProcessorEditor::~Pfmcpp_project10AudioProcessorEditor()
{
    stopTimer();
}

//==============================================================================
void Pfmcpp_project10AudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    g.setColour (Colours::white);
    g.setFont (15.0f);
    g.drawFittedText ("Hello World!", getLocalBounds(), Justification::centred, 1);
}

void Pfmcpp_project10AudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    meter.setBounds(15,30,25,245);

    dbscale.ticks = meter.ticks;
    dbscale.yOffset = meter.getY();
    
    dbscale.setBounds(meter.getRight(), 0, 30, getHeight());
}

void Pfmcpp_project10AudioProcessorEditor::timerCallback()
{
    if( processor.fifoAudioToGUI.pull(pulled) )
    {
        float gaintodB = Decibels::gainToDecibels( pulled.getMagnitude(0, pulled.getNumSamples() ) );
        meter.update(gaintodB);
        DBG( "pulled vaild " );
    }
    else
    {
        DBG( "pulled not valid" );
    }
}

//==============================================================================

void Meter::paint(Graphics& g)
{
    auto h = getHeight();
    auto bounds = getBounds();
    
    level = jmap(level, MindB, MaxdB, 0.f, 1.f);
    
    DBG( level );
    
    g.fillAll(Colours::black);
    g.setColour(Colours::green);
    g.fillRect( bounds.withHeight(h*level).withY(h * (1.f-level)));
}
               
void Meter::update(float passed)
{
    level = passed;
    repaint();
}

void Meter::resized()
{
    ticks.clear();
    int h = getHeight();
    
    for( int i = MindB; i <= MaxdB; ++i )
    {
        if( i % 6 == 0 )
        {
            Tick t;
            t.dB = i;
            t.y = jmap((int)i, (int)MindB, (int)MaxdB, h, 0);
            
            ticks.push_back(t);
        }
    }
}
//==============================================================================

void dBScale::paint(Graphics& g)
{
    g.setColour(Colours::white);
    
    for( Tick t : ticks)
    {
        g.drawSingleLineText(String(t.dB), 0, t.y + yOffset);
    }
}
